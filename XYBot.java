package com.nchl.botmovement.controller;

import java.util.Scanner;

public class XYBot {
    private int x;
    private int y;
    private int direction; // 0: North, 1: East, 2: South, 3: West

    public XYBot() {
        this.x = 0;
        this.y = 0;
        this.direction = 0; // Initial direction: North
    }

    public void move(String command) {
        char action = command.charAt(0);

        // Extract the substring starting from index 1 to the end (to get the steps)
        String stepsString = command.substring(1);

        if (stepsString.isEmpty() || !stepsString.matches("\\d+")) {
            System.out.println("Invalid command format. Please use 'L', 'R', or 'M' followed by the number of steps.");
            return;
        }

        int steps = Integer.parseInt(stepsString);

        switch (action) {
            case 'L':
                turnLeft();
                break;
            case 'R':
                turnRight();
                break;
            case 'M':
                moveForward(steps);
                break;
            default:
                System.out.println("Invalid command. Please use 'L', 'R', or 'M' followed by the number of steps.");
        }
    }

    private void turnLeft() {
        direction = (direction + 3) % 4; // 90-degree counterclockwise turn
    }

    private void turnRight() {
        direction = (direction + 1) % 4; // 90-degree clockwise turn
    }

    private void moveForward(int steps) {
        switch (direction) {
            case 0:
                y += steps; // Move North
                break;
            case 1:
                x += steps; // Move East
                break;
            case 2:
                y -= steps; // Move South
                break;
            case 3:
                x -= steps; // Move West
                break;
        }
    }

    public void displayLocation() {
        String currentDirection = getDirectionString();
        System.out.println("Current Location: (" + x + ", " + y + "), Facing " + currentDirection);
    }

    private String getDirectionString() {
        switch (direction) {
            case 0:
                return "North";
            case 1:
                return "East";
            case 2:
                return "South";
            case 3:
                return "West";
            default:
                return "Unknown";
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        XYBot bot = new XYBot();

        System.out.println("Welcome to the XY Bot Program!");

        while (true) {
            System.out.print("Enter movement command ('L', 'R', 'M', or 'exit' to end): ");
            String command = scanner.nextLine();

            if (command.equalsIgnoreCase("exit")) {
                System.out.println("Program terminated. Final location:");
                bot.displayLocation();
                break;
            }

            bot.move(command);
            bot.displayLocation();
        }

        scanner.close();
    }

}
